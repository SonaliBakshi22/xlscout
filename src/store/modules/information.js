import axios from "axios"
const state = {
    information:[]
    };
    const getters = {
        allInformation: state => state.information
    };
    const actions = {
        async getInformation({commit}){
            const response = await axios.get("http://localhost:3000/information")
            commit("setInformation",response.data);
        },
        async addInformation({commit},info){
            const response = await axios.post("http://localhost:3000/information",info)
            commit("newInformation",response.data);
        } 
    };
    const mutations = {
        setInformation: (state,information) => (state.information = information),
        newInformation: (state,info) => (state.information.unshift(info))
    };
    
    export default{
        state,
        getters,
        actions,
        mutations
    }