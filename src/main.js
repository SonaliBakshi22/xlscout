import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import VueRouter from 'vue-router'
import corpus from './components/corpus.vue'
import Login from './components/Login.vue'
import store from './store'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Vuelidate);
Vue.use(VueRouter);
const routes=[
  {path:'/corpus',component:corpus},
  {path:'',component:Login},

]
const router = new VueRouter({
  routes
})

new Vue({
  router:router,
  store,

  // store:store,
  render: h => h(App)
}).$mount('#app')
